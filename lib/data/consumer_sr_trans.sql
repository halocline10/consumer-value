  select
        SRF.CONSUMER_DIM_ID,
        SRF.SR_FACT_ID,
        SRF.SR_SUBMIT_DATE_DIM_ID,
        DD.CALENDAR_DATE sr_submit_date,
        SRF.SR_DIM_ID,
        SRF.SM_SR_OID,
        SRD.ENTRY_POINT_ID,
        SRF.SR_AFFILIATE_DIM_ID,
        SRF.TASK_DIM_ID,
        SRF.ZIPCODE_DIM_ID,
        SRD.NEW_USER_FG,
        SRF.BRANDED_DIM_ID,
        SRD.GROSS_FG gross_srs,
        SRD.NET_FG net_srs,
        SRD.MATCHED_FG matched_srs,
        SRF.LEAD_CNT num_matches,
        SRF.LEAD_ACCEPTED_CNT accepts,
        SRF.AMT_NET_SUM net_rev
    from
        DM_NA.SR_FACT srf,
        DM_NA.SR_DIM srd,
        DM_NA.DATE_DIM dd
    where
        SRF.SR_SUBMIT_DATE_DIM_ID >= 20140101
        and SRF.SR_SUBMIT_DATE_DIM_ID < 20140102
        and SRF.SR_DIM_ID = SRD.SR_DIM_ID
        and SRF.SR_SUBMIT_DATE_DIM_ID = DD.DATE_DIM_ID
        and SRF.CONSUMER_DIM_ID <> -1

